#!/bin/bash
function tpsc
	case $1 in
		(profile) export TRITON_PROFILE=$2 
			eval "$(triton env)" ;;
		(local-dev) unset DOCKER_CERT_PATH
			unset DOCKER_HOST
			unset DOCKER_TLS_VERIFY
			unset COMPOSE_HTTP_TIMEOUT
			unset SDC_ACCOUNT
			unset SDC_KEY_ID
			unset SDC_URL ;;
		(*) echo "Usage: tpsc profile <name> or local-dev"
			echo "  Profile for a specific environment (e.g. prod-optiturn)"
			echo "  or local-dev to clear your env variables." ;;
	esac
end
