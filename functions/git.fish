function push
    if test -n "$argv"
        if ! command git push "$argv"
            set branch (git push 2>&1 >/dev/null | grep git | cut -d " " -f 9)
            command git push --set-upstream origin "$branch" "$argv"
        end
    else
        if ! command git push
            set branch (git push 2>&1 >/dev/null | grep git | cut -d " " -f 9)
            command git push --set-upstream origin "$branch"
        end
    end
end
alias sts="git status"
alias gsave="git stash save"
alias gpop="git stash pop"
alias cmaster="git checkout master && git pull"
alias cnewb="git checkout -b "
alias db="git branch -D "
alias check="git checkout"
