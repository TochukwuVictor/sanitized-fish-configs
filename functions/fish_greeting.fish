function fish_greeting
  set_color $fish_color_autosuggestion
  echo '
          ▄██████████████████████▄    
          █                      █    
          █ ▄██████████████████▄ █    
          █ █                  █ █    
          █ █ ▀▀▄          ▄▀▀ █ █   What if I put some knuckle in your eyeballs?
          █ █  █            █  █ █   Would that help you think?
          █ █                  █ █  /
          █ █       ▀▀▀▀       █ █     
          █ █                  █ █    
          █ █                  █ █    
       █▌ █ ▀██████████████████▀ █ ▐█ 
       █  █                      █  █ 
       █  █ ████████████     ██  █  █ 
       █  █                      █  █ 
       █  █               ▄      █  █ 
       ▀█▄█   ▐█▌       ▄███▄ ██ █▄█▀ 
         ▀█  █████               █▀   
          █   ▐█▌         ▄██▄   █    
          █              ▐████▌  █    
          █ ▄▄▄ ▄▄▄       ▀██▀   █    
          █                      █    
          ▀██████████████████████▀    
              ██            ██        
              ██            ██        
              ██            ██        
              ██            ██        
             ▐██            ██▌       
'
  set_color normal
end
